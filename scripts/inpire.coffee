# Description:
#   Provides a inspiring poster
#
# Dependencies:
#   request
#
# Configuration:
#   None
#
# Commands:
#   hubot inspire - provides an inspiring poster for you
#
# Author:
#   Hinchable
request = require "request"

module.exports = (robot) ->
  robot.respond /inspire/i, (msg) ->

    url = "http://inspirobot.me/api?generate=true"

    request url, (error, response, body) ->
      if (!error && response.statusCode == 200)
        msg.send body
